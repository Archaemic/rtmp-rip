#!/usr/bin/env python
import os
import os.path
import subprocess

def dumpRtmp(url, fname):
	for failed in range(3):
		if not subprocess.call(['rtmpdump', '-r', url, '-o', fname, '--resume', '-V']):
			break
		try:
			if not os.path.getsize(fname):
				os.remove(fname)
		except:
			pass

def toTs(base, tidy=False):
	subprocess.call(['ffmpeg', '-i', base + '.mp4', '-vcodec', 'copy', '-acodec', 'copy', '-vbsf', 'h264_mp4toannexb', base + '.ts'])
	if tidy:
		os.remove(base + '.mp4')

def joinTs(name, count, tidy=False):
	subprocess.call(['ffmpeg', '-i', 'concat:{}'.format('|'.join(['{}_{}.ts'.format(name, i) for i in range(count)])), '-vcodec', 'copy', '-acodec', 'copy', '-absf', 'aac_adtstoasc', name + '.mp4'])
	if tidy:
		for i in range(count):
			os.remove('{}_{}.ts'.format(name, i)) 

def rip(videos, tidy=True):
	for v in videos:
		for i in range(len(v['urls'])):
			dumpRtmp(v['urls'][i], '{}_{}.mp4'.format(v['name'], i))
			toTs('{}_{}'.format(v['name'], i), False)
		joinTs(v['name'], len(v['urls']), tidy)
